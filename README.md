# Encrypted Notepad

README / 10 April 2019

-----

## Introduction

Encrypted Notepad is a mobile application that encrypts the user's note with the [Rijndael (AES)](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) encryption algorithm and uses authentication with a password or a fingerprint.

## Technical information

Name | Value
--- | ---
Main programming language | Java
Operating system targeted | Android 9.0+ (API v28)

## Getting Started

### Prerequisites

To run this project you'll need:

 - [Android Studio](https://developer.android.com/studio)
 - Java SDK
 - Android SDK

### Launching

Build the project and run.

## Preview

![01](https://gitlab.com/Prologh/encrypted-notepad/raw/master/img/01.png)
![02](https://gitlab.com/Prologh/encrypted-notepad/raw/master/img/02.png)
![03](https://gitlab.com/Prologh/encrypted-notepad/raw/master/img/03.png)

## License

Licensed under MIT. Read full license [here](https://gitlab.com/Prologh/encrypted-notepad/raw/master/LICENSE).

## Credits

**Piotr Wosiek** | [GitLab](https://gitlab.com/Prologh) | [GitHub](https://github.com/Prologh)

### Acknowledgements

Project icon is not mine. I found it on the web and I don't remember exactly where. It's not licensed under the same license as this project is.
