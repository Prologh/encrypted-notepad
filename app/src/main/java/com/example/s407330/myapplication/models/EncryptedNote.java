package com.example.s407330.myapplication.models;

import javax.crypto.spec.IvParameterSpec;

public class EncryptedNote {

    private final byte[] _cyphertext;
    private final byte[] _hmac;
    private final IvParameterSpec _iv;

    public EncryptedNote(IvParameterSpec iv, byte[] cyphertext, byte[] hmac){
        _cyphertext = cyphertext;
        _hmac = hmac;
        _iv = iv;
    }

    public byte[] getCyphertext() {
        return _cyphertext;
    }

    public byte[] getHmac() { return _hmac; }

    public IvParameterSpec getIV() {
        return _iv;
    }
}
