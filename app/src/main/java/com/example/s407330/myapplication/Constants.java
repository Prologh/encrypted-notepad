package com.example.s407330.myapplication;

class Constants {

    static final String MAIN_KEY_HASHING_ALGORITHM = "SHA-256";
    static final int CYPHERTEXT_CHUNK_SIZE = 16;

    static final String ENCRYPTION_ALGORITHM = "AES/CBC/PKCS5Padding";
    static final int ENCRYPTION_KEY_LENGTH = 16;

    static final int IV_LENGTH = 16;

    static final String HMAC_ALGORITHM = "HmacSHA256";
//    static final String HMAC_KEY_HASHING_ALGORITHM = "SHA256";
//    static final String HMAC_KEY_HASHING_ALGORITHM = "HmacSHA256";
    static final int HMAC_LENGTH = 32;

    static final String REPOSITORY_NAME = "BSM";
    static final String REPOSITORY_KEY_NOTE = "encrypted-note";

    private Constants(){}
}
