package com.example.s407330.myapplication;

import java.security.SecureRandom;

import javax.crypto.spec.IvParameterSpec;

public class IVGenerator {

    public IvParameterSpec generateIV(int ivLength) {
        byte[] randomBytesArray = new byte[ivLength];
        new SecureRandom().nextBytes(randomBytesArray);

        return new IvParameterSpec(randomBytesArray);
    }
}
