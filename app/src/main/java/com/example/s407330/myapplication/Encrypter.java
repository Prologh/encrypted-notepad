package com.example.s407330.myapplication;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

class Encrypter {

    byte[] encrypt(IvParameterSpec iv, byte[] valueToEncrypt, SecretKeySpec secretKey) throws Exception {

        // Initialize cipher.
        Cipher cipher = Cipher.getInstance(Constants.ENCRYPTION_ALGORITHM);

        // Set-up encryption mode with secret key and initialisation vector.
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);

        // Encrypt note.
        return cipher.doFinal(valueToEncrypt);
    }

    byte[] decrypt(IvParameterSpec iv, byte[] encryptedValue, SecretKeySpec secretKey) throws Exception {

        // Initialize cipher.
        Cipher cipher = Cipher.getInstance(Constants.ENCRYPTION_ALGORITHM);

        // Set-up decryption mode with secret key and initialisation vector.
        cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);

        // Decrypt note..
        return cipher.doFinal(encryptedValue);
    }
}
