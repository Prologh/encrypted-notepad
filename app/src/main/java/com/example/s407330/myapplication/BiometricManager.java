package com.example.s407330.myapplication;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.biometrics.BiometricPrompt;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.widget.Toast;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.ECGenParameterSpec;

class BiometricManager {

    private final String KEY_STORE_ENTRY_KEY = "BSM";
    private final String KEY_STORE_ENTRY_SALT = "JYUaX";
    private final String KEY_STORE_NAME = "AndroidKeyStore";
    private final String SIGNATURE_HASHING_ALGORITHM = "SHA256withECDSA";

    private final Context _context;

    BiometricManager(Context context){
        _context = context;
    }

    boolean isFingerprintRegistered() {
        return containsKey(KEY_STORE_ENTRY_KEY);
    }

    void showAuthenticationPrompt(){
        if (!isSupportBiometricPrompt()){
            Toast.makeText(
                    _context,
                    "No fingerprint device detected!",
                    Toast.LENGTH_LONG).show();
            return;
        }

        String message =
                KEY_STORE_ENTRY_KEY +
                ":" +
                KEY_STORE_ENTRY_SALT;
        Signature signature;
        try{
            signature = initSignature(KEY_STORE_ENTRY_KEY);
        } catch (Exception ex){
            Toast.makeText(
                    _context,
                    "Failed to initialize signature!",
                    Toast.LENGTH_LONG).show();
            throw new RuntimeException(ex);
        }
        BiometricPrompt prompt = buildBiometricPrompt();
        CancellationSignal cancellationSignal = getCancellationSignal();
        BiometricPrompt.AuthenticationCallback authenticationCallback = getAuthenticationCallback(message, true);

        if (signature != null){
            prompt.authenticate(new BiometricPrompt.CryptoObject(signature), cancellationSignal, _context.getMainExecutor(), authenticationCallback);
        } else {
            Toast.makeText(
                    _context,
                    "No fingerprint registered!",
                    Toast.LENGTH_LONG).show();
        }
    }

    void showRegistrationPrompt(){
        if (!isSupportBiometricPrompt()){
            Toast.makeText(
                    _context,
                    "No biometric hardware detected!",
                    Toast.LENGTH_LONG).show();
            return;
        }

        String message;
        Signature signature;
        try{
            KeyPair keyPair = generateKeyPair(KEY_STORE_ENTRY_KEY, true);
            message =
                    Base64.encodeToString(keyPair.getPublic().getEncoded(), Base64.URL_SAFE) +
                    ":" +
                    KEY_STORE_ENTRY_KEY +
                    ":" +
                    KEY_STORE_ENTRY_SALT;
            signature = initSignature(KEY_STORE_ENTRY_KEY);
        } catch (Exception ex){
            Toast.makeText(
                    _context,
                    "Failed to initialize signature!",
                    Toast.LENGTH_LONG).show();
            throw new RuntimeException(ex);
        }
        BiometricPrompt prompt = buildBiometricPrompt();
        CancellationSignal cancellationSignal = getCancellationSignal();
        BiometricPrompt.AuthenticationCallback authenticationCallback = getAuthenticationCallback(message, false);

        if (signature != null){
            prompt.authenticate(new BiometricPrompt.CryptoObject(signature), cancellationSignal, _context.getMainExecutor(), authenticationCallback);
        } else {
            Toast.makeText(
                    _context,
                    "No fingerprint registered!",
                    Toast.LENGTH_LONG).show();
        }
    }

    private BiometricPrompt buildBiometricPrompt(){
        return new BiometricPrompt.Builder(_context)
                .setTitle(" ")
                .setSubtitle(" ")
                .setDescription(" ")
                .setNegativeButton("Cancel", _context.getMainExecutor(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing.
                    }
                })
                .build();
    }

    private boolean containsKey(String keyName) {
        try {
            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_NAME);
            keyStore.load(null);
            return keyStore.containsAlias(keyName);
        } catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    private KeyPair generateKeyPair(String keyName, boolean invalidatedByBiometricEnrollment) throws Exception {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_EC, KEY_STORE_NAME);

        KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(keyName,
                KeyProperties.PURPOSE_SIGN)
                .setAlgorithmParameterSpec(new ECGenParameterSpec("secp256r1"))
                .setDigests(KeyProperties.DIGEST_SHA256,
                        KeyProperties.DIGEST_SHA384,
                        KeyProperties.DIGEST_SHA512)
                // Require the user to authenticate with a biometric to authorize every use of the key
                .setUserAuthenticationRequired(true)
                // Generated keys will be invalidated if the biometric templates are added more to user device
                .setInvalidatedByBiometricEnrollment(invalidatedByBiometricEnrollment);

        keyPairGenerator.initialize(builder.build());

        return keyPairGenerator.generateKeyPair();
    }

    private BiometricPrompt.AuthenticationCallback getAuthenticationCallback(final String message, final boolean registration) {
        // Callback for biometric authentication result
        return new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(
                        _context,
                        "Fingerprint authentication error!",
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
                super.onAuthenticationHelp(helpCode, helpString);
                Toast.makeText(
                        _context,
                        "Fingerprint authentication error!",
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAuthenticationSucceeded(BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                Signature signature = result.getCryptoObject().getSignature();
                try {
                    signature.update(message.getBytes());
                    String signatureString = Base64.encodeToString(signature.sign(), Base64.URL_SAFE);
                    // Normally, message and signatureString are sent to the server and then verified

                    // TODO:
                    // What next to do with message and signatureString?

                    showDialog("Authentication succeeded!",
                            "Registration: \n" +
                                    registration + "\n" +
                                    "Signature:\n" + signature +
                                    "Message: " + message);

                } catch (SignatureException e) {
                    throw new RuntimeException();
                }
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(
                        _context,
                        "Fingerprint authentication failed!",
                        Toast.LENGTH_LONG).show();
            }
        };
    }

    private CancellationSignal getCancellationSignal() {
        CancellationSignal cancellationSignal = new CancellationSignal();
        cancellationSignal.setOnCancelListener(new CancellationSignal.OnCancelListener() {
            @Override
            public void onCancel() {
                // Handle cancel result
                Toast.makeText(
                        _context,
                        "Authentication canceled",
                        Toast.LENGTH_SHORT).show();
            }
        });

        return cancellationSignal;
    }

    @Nullable
    private KeyPair getKeyPair(String keyName) throws Exception {
        KeyStore keyStore = KeyStore.getInstance(KEY_STORE_NAME);
        keyStore.load(null);

        if (keyStore.containsAlias(keyName)) {

            // Get public key
            PublicKey publicKey = keyStore.getCertificate(keyName).getPublicKey();

            // Get private key
            PrivateKey privateKey = (PrivateKey) keyStore.getKey(keyName, null);

            // Return a key pair
            return new KeyPair(publicKey, privateKey);
        }

        return null;
    }

    @Nullable
    private Signature initSignature (String keyName) throws Exception {
        KeyPair keyPair = getKeyPair(keyName);

        if (keyPair != null) {
            Signature signature = Signature.getInstance(SIGNATURE_HASHING_ALGORITHM);
            signature.initSign(keyPair.getPrivate());

            return signature;
        }

        return null;
    }

    private boolean isSupportBiometricPrompt() {
        return _context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_FINGERPRINT);
    }

    private void showDialog(String title, String message) {
        new AlertDialog.Builder(_context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("ok", null).show();
    }
}
