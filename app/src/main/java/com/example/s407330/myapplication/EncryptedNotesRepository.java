package com.example.s407330.myapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import com.example.s407330.myapplication.models.EncryptedNote;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

import javax.crypto.spec.IvParameterSpec;

public class EncryptedNotesRepository {

    private final SharedPreferences sharedPreferences;

    public EncryptedNotesRepository(Context context) {
        sharedPreferences = context.getSharedPreferences(Constants.REPOSITORY_NAME, Context.MODE_PRIVATE);
    }

    public void addEncryptedNote(EncryptedNote encryptedNote) throws IOException  {
        addEncryptedNote(encryptedNote.getIV(), encryptedNote.getCyphertext(), encryptedNote.getHmac());
    }

    public void addEncryptedNote(IvParameterSpec iv, byte[] encryptedNote, byte[] hmac) throws IOException  {

        // Concatenate the byte arrays of IV and cyphertext.
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );

        // Write the 16 bytes of IV at the start.
        outputStream.write(iv.getIV());

        // Then write the bytes of cyphertext.
        outputStream.write(encryptedNote);

        // Finally, write the HMAC at the end.
        outputStream.write(hmac);

        // Save the concatenated byte array.
        byte outputByteArray[] = outputStream.toByteArray();

        // Encode the byte array as single Base64 string value.
        String outputString =  Base64.encodeToString(outputByteArray, Base64.DEFAULT);

        // Save it to the shared preferences.
        sharedPreferences.edit()
                .putString(Constants.REPOSITORY_KEY_NOTE, outputString)
                .apply();
    }

    public boolean hasEncryptedNote() {
        return sharedPreferences.contains(Constants.REPOSITORY_KEY_NOTE);
    }

    public EncryptedNote getEncyptedNote() {

        // Get concatenated string value of IV and cyphertext.
        String loadedString = sharedPreferences.getString(Constants.REPOSITORY_KEY_NOTE, null);

        // Decode the string from Base64 to byte array.
        byte[] decodedByteArray = Base64.decode(loadedString, Base64.DEFAULT);

        // Take the start of byte array and convert it to IV.
        byte[] ivByteArray = Arrays.copyOf(decodedByteArray, Constants.IV_LENGTH);
        IvParameterSpec iv = new IvParameterSpec(ivByteArray);

        // Take the rest of byte array - the cyphertext byte array.
        byte[] cyphertextByteArray = Arrays.copyOfRange(decodedByteArray, Constants.IV_LENGTH, decodedByteArray.length - Constants.HMAC_LENGTH);

        // Take the end of byte array - the HMAC byte array.
        byte[] hmac = Arrays.copyOfRange(decodedByteArray, decodedByteArray.length - Constants.HMAC_LENGTH, decodedByteArray.length);

        return new EncryptedNote(iv, cyphertextByteArray, hmac);
    }

    public void removeEncryptedNote() {
        sharedPreferences.edit()
                .remove(Constants.REPOSITORY_KEY_NOTE)
                .apply();
    }
}
