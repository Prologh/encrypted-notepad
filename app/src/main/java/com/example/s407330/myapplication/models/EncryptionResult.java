package com.example.s407330.myapplication.models;

public class EncryptionResult {

    private final boolean _hasSucceeded;
    private final String _output;

    public EncryptionResult(boolean hasSucceeded) {
        this(hasSucceeded, null);
    }

    public EncryptionResult(boolean hasSucceeded, String output) {
        _hasSucceeded = hasSucceeded;
        _output = output;
    }

    public boolean hasOutput() { return _output != null; }

    public boolean hasSucceeded() {
        return _hasSucceeded;
    }

    public String getOutput() {
        return _output;
    }
}
