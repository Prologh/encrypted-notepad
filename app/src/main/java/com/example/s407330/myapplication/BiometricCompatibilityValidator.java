package com.example.s407330.myapplication;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import com.example.s407330.myapplication.models.BiometricCompatibilityResult;

import static android.content.Context.KEYGUARD_SERVICE;

public class BiometricCompatibilityValidator {

    private final Context _context;

    BiometricCompatibilityValidator(Context context){
        _context = context;
    }

    BiometricCompatibilityResult validateCompatibility(){
        KeyguardManager keyguardManager =  (KeyguardManager) _context.getSystemService(KEYGUARD_SERVICE);

        if (!keyguardManager.isKeyguardSecure()) {
            return new BiometricCompatibilityResult("Lock screen security not enabled in Settings");
        }

        if (ActivityCompat.checkSelfPermission(_context, Manifest.permission.USE_BIOMETRIC) != PackageManager.PERMISSION_GRANTED) {
            return new BiometricCompatibilityResult("Biometric permission not enabled");
        }

        return BiometricCompatibilityResult.Success;
    }
}
