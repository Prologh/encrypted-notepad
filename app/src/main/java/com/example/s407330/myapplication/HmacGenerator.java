package com.example.s407330.myapplication;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class HmacGenerator {

    private static final int CYPHERTEXT_CHUNK_SIZE = Constants.CYPHERTEXT_CHUNK_SIZE;

    public byte[] generateHmac(IvParameterSpec iv, byte[] cyphertext, SecretKeySpec hmacKey) throws NoSuchAlgorithmException, InvalidKeyException {

        // Initialize the HMAC with hashed key.
        Mac mac = Mac.getInstance(Constants.HMAC_ALGORITHM);
        mac.init(hmacKey);
        mac.reset();

        // Update for IV.
        // No need for chunking the IV as it is already 16 bytes long.
        mac.update(iv.getIV());

        for (int i = 0; i < cyphertext.length / CYPHERTEXT_CHUNK_SIZE; i++)
        {
            // Split cypher text into chunk.
            byte[] chunk = Arrays.copyOfRange(cyphertext, i * CYPHERTEXT_CHUNK_SIZE, (i + 1) * CYPHERTEXT_CHUNK_SIZE);

            // Update for each chunk of ciphertext.
            mac.update(chunk);
        }

        // Finalize.
        return mac.doFinal();
    }
}
