package com.example.s407330.myapplication;

import android.content.Context;

import com.example.s407330.myapplication.models.*;

import java.util.Arrays;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

class EncryptionManager {

    private final EncryptedNotesRepository _encryptedNotesRepository;
    private final Encrypter _encrypter;
    private final HmacGenerator _hmacGenerator;
    private final IVGenerator _ivGenerator;
    private final Hasher _hasher;

    EncryptionManager(Context context) {
        _encryptedNotesRepository = new EncryptedNotesRepository(context);
        _encrypter = new Encrypter();
        _hmacGenerator = new HmacGenerator();
        _ivGenerator = new IVGenerator();
        _hasher = new Hasher();
    }

    EncryptionResult addNote(String passwordString, String noteString) {
        try {

            // Generate random IV.
            IvParameterSpec iv = _ivGenerator.generateIV(Constants.IV_LENGTH);

            // Generate 32 bytes long hash on base of user provided password.
            byte[] mainKeyByteArray = _hasher.hash(
                    passwordString.getBytes(),
                    Constants.MAIN_KEY_HASHING_ALGORITHM
            );

            // Split the secret key into two 16 byte halves.
            // First half is used for the note encryption.
            // Second one is used for the HMAC generation.
            byte[] noteKeyByteArray = Arrays.copyOf(mainKeyByteArray, Constants.ENCRYPTION_KEY_LENGTH);
            byte[] hmacKeyByteArray = Arrays.copyOfRange(mainKeyByteArray, Constants.ENCRYPTION_KEY_LENGTH, mainKeyByteArray.length);

            // Create keys for encryption and HMAC.
            SecretKeySpec noteKey = new SecretKeySpec(noteKeyByteArray, Constants.ENCRYPTION_ALGORITHM);
            SecretKeySpec hmacKey = new SecretKeySpec(hmacKeyByteArray, Constants.HMAC_ALGORITHM);

            // Pass IV, password and note to encrypter.
            byte[] cyphertext = _encrypter.encrypt(
                    iv,
                    noteString.getBytes(),
                    noteKey
            );

            // Generate HMAC.
            byte[] hmac = _hmacGenerator.generateHmac(iv, cyphertext, hmacKey);

            // Add IV, cypher text add HMAC to repository.
            _encryptedNotesRepository.addEncryptedNote(iv, cyphertext, hmac);

            return new EncryptionResult(true);
        } catch (Exception ex){
            return new EncryptionResult(false);
        }
    }

    boolean hasSavedNote(){
        return _encryptedNotesRepository.hasEncryptedNote();
    }

    EncryptionResult getNote(String passwordString) {
        try {

            // Get cypher text from repository.
            EncryptedNote encyptedNote = _encryptedNotesRepository.getEncyptedNote();

            // Generate 32 bytes long hash on base of user provided password.
            byte[] mainKeyByteArray = _hasher.hash(
                    passwordString.getBytes(),
                    Constants.MAIN_KEY_HASHING_ALGORITHM
            );

            // Split the secret key into two 16 byte halves.
            // First half is used for the note encryption.
            // Second one is used for the HMAC generation.
            byte[] noteKeyByteArray = Arrays.copyOf(mainKeyByteArray, Constants.ENCRYPTION_KEY_LENGTH);
            byte[] hmacKeyByteArray = Arrays.copyOfRange(mainKeyByteArray, Constants.ENCRYPTION_KEY_LENGTH, mainKeyByteArray.length);

            // Create keys for encryption and HMAC.
            SecretKeySpec noteKey = new SecretKeySpec(noteKeyByteArray, Constants.ENCRYPTION_ALGORITHM);
            SecretKeySpec hmacKey = new SecretKeySpec(hmacKeyByteArray, Constants.HMAC_ALGORITHM);

            // Generate HMAC.
            byte[] hmac = _hmacGenerator.generateHmac(encyptedNote.getIV(), encyptedNote.getCyphertext(), hmacKey);

            // Before decryption, check HMAC integrity.
            if (!Arrays.equals(hmac, encyptedNote.getHmac())){
                return new EncryptionResult(false);
            }

            // Pass loaded IV, password and cyphertext to encrypter.
            byte[] decryptedByteArray = _encrypter.decrypt(
                    encyptedNote.getIV(),
                    encyptedNote.getCyphertext(),
                    noteKey
            );

            // Construct new string instance note from decrypted byte array.
            String decryptedNote = new String(decryptedByteArray);

            return new EncryptionResult(true, decryptedNote);
        } catch (Exception ex) {
            return new EncryptionResult(false);
        }
    }

    void removeNote() {
        _encryptedNotesRepository.removeEncryptedNote();
    }
}
