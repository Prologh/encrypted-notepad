package com.example.s407330.myapplication;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

class Hasher {

    byte[] hash(byte[] input, String hashingAlgorithm) throws NoSuchAlgorithmException {

        // Initialize.
        MessageDigest sha = MessageDigest.getInstance(hashingAlgorithm);

        // Call hash function.
        return sha.digest(input);
    }
}
