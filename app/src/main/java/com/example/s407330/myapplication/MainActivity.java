package com.example.s407330.myapplication;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.s407330.myapplication.models.BiometricCompatibilityResult;
import com.example.s407330.myapplication.models.EncryptionResult;

public class MainActivity extends AppCompatActivity {

    TextView noteLabel;
    TextView passwordLabel;
    TextView newPasswordLabel;
    EditText noteTextArea;
    EditText passwordInput;
    EditText newPasswordInput;
    Button buttonSubmit;
    Button buttonRemove;
    Button buttonChangePassword;
    Button buttonFingerprint;

    private BiometricManager _biometricManager;
    private BiometricCompatibilityValidator _biometricCompatibilityValidator;
    private EncryptionManager _encryptionManager;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _biometricManager = new BiometricManager(this);
        _biometricCompatibilityValidator = new BiometricCompatibilityValidator(this);
        _encryptionManager = new EncryptionManager(this);

        noteLabel = findViewById(R.id.noteLabel);
        passwordLabel = findViewById(R.id.passwordLabel);
        newPasswordLabel = findViewById(R.id.newPasswordLabel);

        noteTextArea = findViewById(R.id.noteTextArea);
        passwordInput = findViewById(R.id.passwordInput);
        newPasswordInput = findViewById(R.id.newPasswordInput);

        buttonSubmit = findViewById(R.id.buttonSubmit);
        buttonRemove = findViewById(R.id.buttonRemove);
        buttonChangePassword = findViewById(R.id.buttonChangePassword);
        buttonFingerprint = findViewById(R.id.buttonFingerprint);

        BiometricCompatibilityResult result = _biometricCompatibilityValidator.validateCompatibility();
        if (!result.isSuccessful()) {
            showDialog("Error", result.getMessage());
        }

        buttonFingerprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_encryptionManager.hasSavedNote()){
                    _biometricManager.showAuthenticationPrompt();
                } else {
                    _biometricManager.showRegistrationPrompt();
                }
            }
        });

        toggleNoteControls(_encryptionManager.hasSavedNote());

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!_encryptionManager.hasSavedNote()) {
                    if (noteTextArea.getText().toString().trim().isEmpty()) {
                        showDialog("Error!", "Note field cannot be left empty!");
                        return;
                    }
                }

                if (passwordInput.getText().toString().trim().isEmpty()) {
                    showDialog("Error!", "Password field cannot be left empty!");
                    return;
                }

                if (!_biometricManager.isFingerprintRegistered()){
                    showDialog("Error!", "No fingerprint registered!");
                    return;
                }

                if (!_encryptionManager.hasSavedNote()) {
                    EncryptionResult result = _encryptionManager.addNote(
                            passwordInput.getText().toString(),
                            noteTextArea.getText().toString());
                    if (result.hasSucceeded()){
                        toggleNoteControls(true);
                        showDialog("Success!", "Note encrypted successfully.");
                    } else {
                        showDialog("Error!", "Something went wrong during the encryption process.");
                    }
                } else {
                    EncryptionResult result = _encryptionManager.getNote(passwordInput.getText().toString());
                    if (result.hasSucceeded()){
                        showDialog("Success!", "Your decrypted note:\n\n" + result.getOutput());
                    } else {
                        _encryptionManager.removeNote();
                        toggleNoteControls(false);
                        showDialog(
                                "Wrong password!",
                                "You fool!\n" +
                                        "Your note has been lost irrevocably!");
                    }
                }

                clearInputs();
            }
        });

        buttonRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _encryptionManager.removeNote();
                toggleNoteControls(false);
                clearInputs();
                showDialog("Success!", "Note removed.");
            }
        });

        buttonChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (passwordInput.getText().toString().trim().isEmpty()) {
                    showDialog("Error!", "Old password field cannot be left empty!");
                    return;
                }

                if (newPasswordInput.getText().toString().trim().isEmpty()) {
                    showDialog("Error!", "New password field cannot be left empty!");
                    return;
                }

                EncryptionResult result = _encryptionManager.getNote(passwordInput.getText().toString());
                if (!result.hasSucceeded()) {
                    _encryptionManager.removeNote();
                    toggleNoteControls(false);
                    showDialog(
                            "Wrong password!",
                            "You fool!\n" +
                                    "Your note has been lost irrevocably!");
                }

                _encryptionManager.removeNote();
                result = _encryptionManager.addNote(newPasswordInput.getText().toString(), result.getOutput());

                if (!result.hasSucceeded()){
                    _encryptionManager.removeNote();
                    toggleNoteControls(false);
                    showDialog(
                            "Wrong password!",
                            "You fool!\n" +
                                    "Your note has been lost irrevocably!");
                }

                showDialog("Success!", "Password changed.");
                clearInputs();
            }
        });
    }

    private void clearInputs() {
        noteTextArea.setText("");
        passwordInput.setText("");
        newPasswordInput.setText("");
        if (noteTextArea.getVisibility() == View.VISIBLE) {
            noteTextArea.requestFocus();
        } else {
            passwordInput.requestFocus();
        }
    }

    private void toggleNoteControls(boolean isNoteSaved) {
        if (isNoteSaved) {
            noteLabel.setVisibility(View.GONE);
            buttonSubmit.setText("Decrypt with password");
            buttonFingerprint.setText("Decrypt with fingerprint");
            noteTextArea.setVisibility(View.GONE);
            buttonRemove.setVisibility(View.VISIBLE);
            newPasswordLabel.setVisibility(View.VISIBLE);
            newPasswordInput.setVisibility(View.VISIBLE);
            buttonChangePassword.setVisibility(View.VISIBLE);
        } else {
            noteLabel.setVisibility(View.VISIBLE);
            buttonSubmit.setText("Encrypt note");
            buttonFingerprint.setText("Register fingerprint");
            noteTextArea.setVisibility(View.VISIBLE);
            buttonRemove.setVisibility(View.GONE);
            newPasswordLabel.setVisibility(View.GONE);
            newPasswordInput.setVisibility(View.GONE);
            buttonChangePassword.setVisibility(View.GONE);

            noteTextArea.requestFocus();
        }
    }

    private void showDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("ok", null).show();
    }
}
