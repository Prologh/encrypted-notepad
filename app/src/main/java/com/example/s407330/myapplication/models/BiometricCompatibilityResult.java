package com.example.s407330.myapplication.models;

public class BiometricCompatibilityResult {

    public final static BiometricCompatibilityResult Success = new BiometricCompatibilityResult();

    private String message;

    public BiometricCompatibilityResult() {

    }

    public BiometricCompatibilityResult(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccessful(){
        return this.equals(Success);
    }
}
